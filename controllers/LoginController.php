<?php

namespace app\controllers;

use yii;
use app\models\Authentication;
use yii\web\Response;

class LoginController extends \yii\web\Controller
{
  public $enableCsrfValidation = false;
    public function actionLogin()
    {
      $request = yii::$app->request;  //  Creating instance that handle user request

      //  getting userName and password from user login request
      $userName = $request->post('userName');
      $password = $request->post('password');
      $hasedPassword = md5($password);

      $model = new Authentication();
      Yii::$app->response->format = Response::FORMAT_JSON;
      /*return false if user name and password is not
      match and return accessToken if user name and password is matched*/
      $accessTokenValue = $model->login($userName, $hasedPassword);
      if ($accessTokenValue == false) {
        //  return value when user name and password is wrong
        return ['success' => false,
                    'accessToken' => null];
      } else {
        //  return value when user name and password is true
        return ['success' => true,
                    'accessToken' => $accessTokenValue];
      }
        // return $this->render('login');
    }

}