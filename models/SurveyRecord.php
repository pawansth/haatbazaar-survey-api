<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "surveyRecord".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $mobileNo
 * @property string $jobProfile
 * @property string $natureBusiness
 * @property string $officeAddress
 * @property string $officeContact
 * @property string $birthDate
 * @property string $opinion
 * @property string $buyingNeeds
 * @property string $image
 */
class SurveyRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'surveyRecord';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'mobileNo'], 'required'],
            [['buyingNeeds'], 'string'],
            [['name', 'mobileNo', 'jobProfile', 'officeContact', 'image'], 'string', 'max' => 50],
            [['address', 'natureBusiness', 'officeAddress'], 'string', 'max' => 100],
            [['birthDate', 'opinion'], 'string', 'max' => 20],
            [['file'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'mobileNo' => 'Mobile No',
            'jobProfile' => 'Job Profile',
            'natureBusiness' => 'Nature Business',
            'officeAddress' => 'Office Address',
            'officeContact' => 'Office Contact',
            'birthDate' => 'Birth Date',
            'opinion' => 'Opinion',
            'buyingNeeds' => 'Buying Needs',
            'image' => 'Image',
            'file' => 'Add Image'
        ];
    }

        public function getAuthentication() {
        return $this->hasOne(Authentication::className(), ['id' => 'surveyor']);
    }
}
