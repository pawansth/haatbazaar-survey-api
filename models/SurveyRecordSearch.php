<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SurveyRecord;

/**
 * SurveyRecordSearch represents the model behind the search form about `app\models\SurveyRecord`.
 */
class SurveyRecordSearch extends SurveyRecord
{
     public $authentication;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','surveyor'], 'integer'],
            [['name', 'address', 'mobileNo', 'jobProfile', 'natureBusiness', 'officeAddress', 'officeContact', 'birthDate', 'opinion', 'buyingNeeds', 'image','authentication'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SurveyRecord::find();
            $query->joinWith(['authentication']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

            $dataProvider->sort->attributes['authentication'] = [
            'asc' => ['authentication.userName' => SORT_ASC],
            'desc' => ['authentication.userName' => SORT_DESC]
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            
            ->andFilterWhere(['like', 'surveyor', $this->surveyor])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'mobileNo', $this->mobileNo])
            ->andFilterWhere(['like', 'jobProfile', $this->jobProfile])
            ->andFilterWhere(['like', 'natureBusiness', $this->natureBusiness])
            ->andFilterWhere(['like', 'officeAddress', $this->officeAddress])
            ->andFilterWhere(['like', 'officeContact', $this->officeContact])
            ->andFilterWhere(['like', 'birthDate', $this->birthDate])
            ->andFilterWhere(['like', 'opinion', $this->opinion])
            ->andFilterWhere(['like', 'buyingNeeds', $this->buyingNeeds])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'authentication.userName', $this->authentication]); 

        return $dataProvider;
    }
}
